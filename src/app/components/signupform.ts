import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { countries } from '../helpers/countries';

@Component({
  templateUrl: './signupform.html',
})
export class SignUpComponent {
  signUpForm: FormGroup;
  submitTried: boolean = false;
  countries;
  constructor() {
    this.signUpForm = new FormGroup({
      ime: new FormControl('', [Validators.required,Validators.minLength(3)]),
      prezime: new FormControl('', Validators.required),
      pol: new FormControl('muski', Validators.required),
      adresa: new FormGroup({
        drzava: new FormControl('', Validators.required),
        grad: new FormControl('', Validators.required),
        ulica: new FormControl('', Validators.required),
        broj: new FormControl('', Validators.required),
        postanskiBroj: new FormControl('', Validators.required)
      }),
      email: new FormControl('', [Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      razumem: new FormControl(false, Validators.requiredTrue)
    })

    

    this.countries = countries;

  }

  submitujFormu() {
    this.submitTried = true;
  }

}
