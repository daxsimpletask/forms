import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { countries } from '../helpers/countries';

@Component({
  templateUrl: './dynamicform.html',
})
export class DynamicFormComponent {
  testForm: FormGroup;
  submitTried: boolean = false;
  countries  = countries;
  formConfig = [
      {
        label:'Ime',
        key: 'ime',
        validators:['required'],
        type:'text'
      },
      {
        label:'Prezime',
        key: 'prezime',
        validators:['required'],
        type:'text' 
      },
      {
        label:'Prezime',
        key: 'prezime',
        validators:['required'],
        type:'select',
        selectPlaceholder:'Choose country',
        options:countries
      },
      {
        label:'Email',
        key: 'email',
        validators:['required'],
        type:'text' 
      },
      {
        label:'Novi field',
        key: 'novifield',
        validators:['required'],
        type:'text' 
      },
      {
        type:'subgroup',
        name: 'adresa',
        childControls:[
          {
            label:'Ulica',
            key: 'ulica',
            validators:['required'] 
          },
          {
            label:'Grad',
            key: 'grad',
            validators:['required'] 
          }
        ]
      }
  ]

  constructor() {
    let testFormObject = {};
    var arrayValidatora = [];
    for(var i=0;i< this.formConfig.length;i++){
      if(this.formConfig[i].type !== 'subgroup'){
        arrayValidatora = [];
        for(var j=0;j<this.formConfig[i].validators.length;j++){
          arrayValidatora.push(Validators[this.formConfig[i].validators[j]]);
        }
        testFormObject[this.formConfig[i].key] = new FormControl('',arrayValidatora);
      }else{
        var subgroupObject = {}
        for(var k=0;k< this.formConfig[i].childControls.length;k++){
            arrayValidatora = [];
            for(var j=0;j<this.formConfig[i].childControls[k].validators.length;j++){
              arrayValidatora.push(Validators[this.formConfig[i].childControls[k].validators[j]]);
            }
            subgroupObject[this.formConfig[i].childControls[k].key] = new FormControl('',arrayValidatora);
        }
        testFormObject[this.formConfig[i].name] = new FormGroup(subgroupObject);
      }
     
    }
    this.testForm = new FormGroup(testFormObject);
    console.log(this.testForm.value)
  }

  submitujFormu() {
    this.submitTried = true;
    console.log(this.testForm.value)
  }

}
