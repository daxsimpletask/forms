import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SignUpComponent } from './components/signupform';
import { DynamicFormComponent } from './components/dynamicform';
import { TestFormComponent } from './components/testform';

const appRoutes: Routes = [
  {path:'', component:SignUpComponent},
  {path:'test', component:DynamicFormComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    TestFormComponent,
    DynamicFormComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
